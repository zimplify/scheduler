<?php
    namespace Zimplify\Scheduling;
    use Zimplify\Core\{Application, Instance};
    use Zimplify\Core\Services\ClassUtils;
    use \DateTime;
    use \RuntimeException;

    /**
     * the Executable task is the core of all forkable task within the system
     * @package Zimplify\Scheduling (code 4)
     * @type instance (code 1)
     * @file Exevutable (code 01)
     */
    abstract class Executable extends Instance {

        const CFG_LOG_FILE_DIR = "system.scheduler.log-dir";
        const CFG_DEBUG_MODE = "system.scheduler.debug";
        const DEF_CONTEXT_TRACE = "3";
        const DEF_CONTEXT_INFO = "2";
        const DEF_CONTEXT_WARNING = "1";
        const DEF_CONTEXT_ERROR = "0";
        const DEF_DATA_BASE = "TKOB_DATA";
        const ERR_FILE_NOTFOUND = 4041121001;
        const ERR_NO_METHOD = 4041121002;
        const ERR_NO_INSTANCE = 4041121003;
        const FLD_ACTIVE = "active";
        const FLD_ASYNC = "async";
        const FLD_METHOD = "action.mehtod";
        const FLD_SOURCE = "action.source";

        private $debug;
        private $logfile;

        /**
         * get the base field 
         * @return string
         */
        protected function base() : string {
            return self::DEF_DATA_BASE;
        }

        /**
         * logging trace message
         * @param string $message the details to log
         * @return void
         */
        protected function debug(string $message) : void {
            if ($this->debug) 
                $this->log($message, self::DEF_CONTEXT_TRACE);            
        }

        /**
         * disable the process from executing
         * @return Executable
         */
        public function disable() : self {
            if ($this->status) {
            $this->status = false;
            $this->save();
            }
            return $this;
        }

        /**
         * enable the process from executing
         * @return Executable
         */
        public function enable() : self {
            if (!$this->status) {
                $this->status = true;
                $this->save();
            }
            return $this;
        }

        /**
         * execute the task real work
         * @return mixed
         */
        protected abstract function execute();

        /**
         * logging errors or events onto a specific file
         * @param string $message the message to write
         * @param int $context (optional) the nature of the message
         * @return void
         */
        protected function log(string $message, int $context = self::DEF_CONTEXT_ERROR) : void {
            $item = "[".(new DateTime())->format("U")."] ";
            switch ($context) {
                case self::DEF_CONTEXT_TRACE:
                    $item .= "TRACE (".$this->type.")>>";
                    break;
                case self::DEF_CONTEXT_INFO:
                    $item .= "INFO (".$this->type.")>>";
                    break;
                case self::DEF_CONTEXT_WARNING:
                    $item .= "WARN (".$this->type.")>>";
                    break;
                case self::DEF_CONTEXT_ERROR:
                    $item .= "ERROR (".$this->type.")>>";
                    break;
            }            
            $item .= "$message\n";
            file_put_contents($this->logfile, $item, FILE_APPEND);
        }

        /**
         * the overriding prepare method
         * @return void
         */
        protected function prepare() {
            parent::prepare();

            // loading the name params
            $instance = null;
            $dir = Application::env(self::CFG_LOG_FILE_DIR);
            $application = ClassUtils::name($this, $instance);

            // fixing our log file for this execution
            $this->logfile = $dir.$instance."-".$this->id."-".(new DateTime)->format("U").".log";

            // now determine debug mode
            $this->debug = Application::env(self::CFG_DEBUG_MODE);
        }

        /**
         * the overriding presave method
         * @return void
         */
        protected function presave() {
            parent::presave();
        }

        /**
         * the main method that forks the process
         * @return mixed
         */
        protected function run() {
            try {
                $result = $this->execute();
                $this->debug("RESULT = ".json_encode($result));
                return $result;
            } catch (Exception $ex) {
                $this->log($ex->getMessage(), self::DEF_CONTEXT_ERROR);
                throw $ex;
            }
        }
    }