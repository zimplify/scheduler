<?php
    namespace Zimplify\Core;
    use Zimplify\Core\Executable;
    use \RuntimeException;

    /**
     * the Schedulable is a task that can be triggered and fulfilled based on time
     * @package Zimplify\Core (code 4)
     * @type instance (code 1)
     * @file Schedulable (code 02)
     */
    abstract class ScheduleTask extends Executable {

        /**
         * execute the fucntion with the testing scheduler
         * @return mixed
         */        
        protected function executable() {
            if (isScheduleActive())
                try {
                    $this->active = true;
                    $result = $this->save()->operate();
                    $this->active = false;
                    $this->save();
                    return $result;
                } catch (Exception $ex) {
                    $this->active = false;
                    $this->save();
                    throw $ex;
                }
            return null;   
        }

        /**
         * check if the schedule is applicable
         * @return bool
         */
        protected function isScheduleActive() : bool {
            $result = true;
            $now = new DateTime();
            
            // now authenticate the time
            foreach ($this->schedule as $period => $value) {
                switch ($period) {
                    case "month":
                        $result = $now->format("n") == $value;
                        break;
                    case "day":
                        $result = $now->format("j") == $value;
                        break;
                    case "dow":
                        $result = $now->format("N") == $value;
                        break;
                    case "hour":
                        $result = $now->format("G") == $value;
                        break;
                    case "minute": 
                        $result = $now->format("i") == $value;
                        break;
                    default:
                        throw new RuntimeException("Unknown schedule parameter.");
                }
                if (!$result) break;
            }
                
            
            return $result;
        }
        
        /**
         * execute the task real work
         * @return mixed
         */         
        protected abstract function operate();

    }