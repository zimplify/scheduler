<?php
    use Zimplify\Core\{Application, Query};

    // make sure our auto load is there...
    require "../../../autoload.php";

    // make sure we have args on the ID
    if (count($argv) > 1) 
        // check if our task us there
        if (count($tasks = Application::search([Query::SRF_ID => $argv[1]], "tasks")) > 0) {
            $task = $tasks[0];
            $task->run();
        } else 
            exit(-1);
    else 
        exit(-1);

    // just exit after that...
    exit(0);
