<?php

    use Zimplify\Core\{Application, Query};
    use Zimplify\Scheduling\Executable;
    use \DateTime;

    // make sure our auto load is there...
    require "../../../autoload.php";

    // some of our constant
    define(STOP_FILE, "../../../../schedule.terminate");
    define(RES_FORCE_STOP, 99);

    /**
     * check if the schedule loop is active
     * @return bool
     */
    function active() : bool {
        return !file_exists(STOP_FILE);
    }

    // this is our main loop
    while (active()) {
        $tasks = Application::search([Query::SRF_STATUS => true, Executable::FLD_ACTIVE => false], "tasks");
        foreach ($tasks as $task) 
            $task->run();
        
        // once finish, let's pause a little
        sleep(300);
    }

    unlink(STOP_FILE);
    $result = RES_FORCE_STOP;

    // now breakout with result
    exit($result);