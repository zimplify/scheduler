<?php
    namespace Zimplify\Scheduing;
    use Zimplify\Scheduing\Executable;
    use \RuntimeException;

    /**
     * the Schedulable is a task that can be triggered and fulfilled based on time
     * @package Zimplify\Scheduing (code 4)
     * @type instance (code 1)
     * @file Queueable (code 02)
     */
    abstract class Queueable extends Executable {

        

        /**
         * check the queue and see if there are items available
         * @return bool
         */
        protected function isQueueActive() : bool {

        }        
    }